#Method 3
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from pymongo import MongoClient
uri = "Professor if you choose to run Please let us know,will share URL for AWS";

client = MongoClient(uri)
db = client.immibytes
#input_data = db.myCollection# comment when using both local & AWS
#cursor=db['myCollection'].find({})
#list(db.myCollection.find({}))


# In[4]:


#Connecting to Mongo-Local
#df =  pd.DataFrame(list(db.myCollection.find({})))
import pymongo
import pandas as pd
#from pymongo import Connection
from pymongo import MongoClient
c = MongoClient()
#from mogo import connect as PyConnection
#connection = Connection()
db = c.immibytes

#input_data = db.transaction_50k.find({"PROD_CODE_40":"D00001"})

#input_data = db.transaction_50k


# In[5]:



#Data frame creation
UP=db.transaction_50k.distinct( "PROD_CODE" )

PUP=pd.DataFrame(list(UP))


# In[ ]:


import numpy as np
import scipy as spy
import sklearn as kit
import pandas as pandas
import statsmodels.api as sm
import matplotlib.pyplot as plot
from pandas.tools import plotting

from statsmodels.formula.api import ols
from sklearn.svm import SVR

from math import sqrt
from sklearn.metrics import mean_squared_error

n=100
rbfTest = [0] * n
LinTest = [0] * n
polyTest = [0] * n
#aList = [0] * n
#bList = [0] * n
for i in range(0, n): # This can come later
      
    # Deriving the column needed
    y=PUP.iloc[i,0]
    input_data = db.transaction_50k.find({"PROD_CODE":y})
    PD1=pd.DataFrame(list(input_data))
    PD1['PRICE']=PD1['SPEND']/PD1['QUANTITY']

    #Extracting the columns needed for modelling
    PD1=PD1[['BASKET_DOMINANT_MISSION','BASKET_PRICE_SENSITIVITY','BASKET_SIZE','BASKET_TYPE','CUST_CODE','CUST_LIFESTAGE',
     'CUST_PRICE_SENSITIVITY','SHOP_DATE','SHOP_HOUR','SHOP_WEEK','SHOP_WEEKDAY','STORE_CODE','STORE_FORMAT',
     'STORE_REGION','QUANTITY','PRICE']]


    # Creation of Dummy Variables

    BASKET_DM_d= pd.get_dummies(PD1.BASKET_DOMINANT_MISSION, prefix='BDM').iloc[:, 1:]
    PD1 = pd.concat([PD1, BASKET_DM_d], axis=1) # BASKET_DOMINANT_MISSION
    #PD1.head()

    BASKET_PS_d= pd.get_dummies(PD1.BASKET_PRICE_SENSITIVITY, prefix='BPS').iloc[:, 1:]
    PD1 = pd.concat([PD1, BASKET_PS_d], axis=1) # BASKET_PRICE_SENSITIVITY

    BASKET_SIZE_d= pd.get_dummies(PD1.BASKET_SIZE, prefix='BS').iloc[:, 1:]# BASKET_SIZE
    PD1 = pd.concat([PD1, BASKET_SIZE_d], axis=1)

    BASKET_TYPE_d= pd.get_dummies(PD1.BASKET_TYPE, prefix='BT').iloc[:, 1:]# BASKET_TYPE
    PD1 = pd.concat([PD1, BASKET_TYPE_d], axis=1)

    CUST_LIFESTAGE_d= pd.get_dummies(PD1.CUST_LIFESTAGE, prefix='CL').iloc[:, 1:]# CUST_LIFESTAGE
    PD1 = pd.concat([PD1, CUST_LIFESTAGE_d], axis=1)

    CUST_PRICE_SENSITIVITY_d= pd.get_dummies(PD1.CUST_PRICE_SENSITIVITY, prefix='CPS').iloc[:, 1:]
    PD1 = pd.concat([PD1, CUST_PRICE_SENSITIVITY_d], axis=1) # CUST_PRICE_SENSITIVITY

    SHOP_HOUR_d= pd.get_dummies(PD1.SHOP_HOUR, prefix='SH').iloc[:, 1:]# SHOP_HOUR
    PD1 = pd.concat([PD1, SHOP_HOUR_d], axis=1)

    SHOP_WEEKDAY_d= pd.get_dummies(PD1.SHOP_WEEKDAY, prefix='SW').iloc[:, 1:]# SHOP_WEEKDAY
    PD1 = pd.concat([PD1, SHOP_WEEKDAY_d], axis=1)

    STORE_CODE_d= pd.get_dummies(PD1.STORE_CODE, prefix='SC').iloc[:, 1:]# STORE_CODE
    PD1 = pd.concat([PD1, STORE_CODE_d], axis=1)

    STORE_FORMAT_d= pd.get_dummies(PD1.STORE_FORMAT, prefix='SF').iloc[:, 1:]# STORE_FORMAT
    PD1 = pd.concat([PD1, STORE_FORMAT_d], axis=1)

    STORE_REGION_d= pd.get_dummies(PD1.STORE_REGION, prefix='SR').iloc[:, 1:]# STORE_REGION
    PD1 = pd.concat([PD1, STORE_REGION_d], axis=1)

    PD1.fillna(0)

    # Removing Columns

    #train = train.drop('BPS_UM','BPS_UM', 1)
    #train.drop(['BPS_UM','BPS_UM','B_Grocery'], axis=1, inplace=True)
    #train.drop(['B_Grocery','B_Mixed','B_Nonfood','BPS_MM'], axis=1, inplace=True)

    # Dropping Subset columns
    PD1=PD1.drop(PD1.columns[0:14], axis=1)
    #PD1.drop(PD1.columns[0:2], axis=1)
    cols = PD1.columns.tolist()
    cols=cols[2:]+cols[:2]
    feature_cols=cols[:-1]

    # Configuring the Training & Test Datasets

    from sklearn.cross_validation import train_test_split

    train, test = train_test_split(PD1, test_size = 0.2)


    # Storing the Dependent & Independent Variables as concise vectors
    X=train[feature_cols]
    y=train.PRICE
    Xtest=test[feature_cols]
    ytest=test.PRICE
    
    '''
    #SVR
    clf = SVR(C=1.0, epsilon=0.2)
    clf.fit(X, y) 
    SVR(C=1.0, cache_size=200, coef0=0.0, degree=3, epsilon=0.2, gamma='auto',
    kernel='rbf', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
    
    SVR.fit(X, y).predict(X)
    '''
    svr_rbf = SVR(kernel='rbf', C=1e3, gamma=0.1)
    svr_lin = SVR(kernel='linear', C=1e3)
    svr_poly = SVR(kernel='poly', C=1e3, degree=2)
    y_rbf = svr_rbf.fit(X, y).predict(X)
    y_lin = svr_lin.fit(X, y).predict(X)
    y_poly = svr_poly.fit(X, y).predict(X)
    rbfTest[i] = np.sqrt(mean_squared_error(y, y_rbf))
    linTest[i] = np.sqrt(mean_squared_error(y, y_lin))
    polyTest[i] = np.sqrt(mean_squared_error(y, y_poly))
   


# In[31]:


rbfTest
LinTest
polyTest

#!/usr/bin/env python
# coding: utf-8

# In[15]:


#Connecting to Mongo-AWS--Professor if you choose to run Please let us know,will share URL for AWS
#Method 3
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from pymongo import MongoClient
uri = "Professor if you choose to run Please let us know,will share URL for AWS";
client = MongoClient(uri)
db = client.immibytes
#input_data = db.myCollection# comment when using both local & AWS
#cursor=db['myCollection'].find({})
#list(db.myCollection.find({}))


# In[38]:


#Connecting to Mongo-Local
#df =  pd.DataFrame(list(db.myCollection.find({})))
import pymongo
import pandas as pd
#from pymongo import Connection
from pymongo import MongoClient
c = MongoClient()
#from mogo import connect as PyConnection
#connection = Connection()
db = c.immibytes

#input_data = db.transaction_50k.find({"PROD_CODE_40":"D00001"})

#input_data = db.transaction_50k


# In[39]:



#Data frame creation
UP=db.transaction_50k.distinct( "PROD_CODE" )

PUP=pd.DataFrame(list(UP))